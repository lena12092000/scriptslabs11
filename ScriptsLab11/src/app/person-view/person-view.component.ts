import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Person } from '../shared/models/person.model';

@Component({
  selector: 'app-person-view',
  templateUrl: './person-view.component.html',
  styleUrls: ['./person-view.component.css']
})
export class PersonViewComponent implements OnInit {
  @Input() inPerson: Person;
  @Output() delete = new EventEmitter<number>();
  @Output() edit = new EventEmitter<Person>();
  nowEdit:boolean;
  constructor() { }

  ngOnInit() {
    console.log(this.inPerson);
    this.nowEdit = false;
  }
  onDeletePerson() {
      this.delete.emit(this.inPerson.id);
    }

    
 onEditPerson(firstname: string = this.inPerson.firstname, lastname: string = this.inPerson.lastname) {
  if (firstname !== "" && lastname !== "") {
    let person = new Person(firstname, lastname, this.inPerson.id);
    this.edit.emit(person);
    this.Edit();
  }
 }
 Edit() {
  this.nowEdit = !this.nowEdit;
}
   
}
